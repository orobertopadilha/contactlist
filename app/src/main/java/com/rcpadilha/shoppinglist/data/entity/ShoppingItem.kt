package com.rcpadilha.shoppinglist.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "shopping_items")
data class ShoppingItem(
    var description: String
) {

    @PrimaryKey(autoGenerate = true)
    var id: Int? = null

}